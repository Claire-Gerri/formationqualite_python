from src.calculator import add
import pytest

def test_add():
    # Arrange + Act
    result = add(7,3)
    # Assert
    assert result == 10

def test_add_string():
    with pytest.raises(TypeError):
        add("7",3)