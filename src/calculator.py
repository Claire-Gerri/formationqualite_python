def add(a,b):
    checkInputs(a,b)
    return a+b

def checkInputs(a,b):
    if not isinstance(a,(int, float)) or not isinstance(b,(int, float)):
        raise TypeError ("ERROR : Inputs must be int or float !")